package org.tempuri;

import junit.framework.TestCase;

/**
 * Prueba unitaria a los metodos de consumo de endpoint calculadora
 * @author Ricardo Giraldo
 *
 */
public class CalculatorSoapTest extends TestCase {

	/**
	 * Test a metodo add, suma
	 */
	public void testAdd() {
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		int resultadoSuma = servicio.add(3, 4);
		System.out.println("Resultado suma: "+resultadoSuma);
		assertTrue(resultadoSuma == 7);
	}

	/**
	 * Test a método substract, resta
	 */
	public void testSubtract() {
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		int resultadoSuma = servicio.subtract(14, 9);
		System.out.println("Resultado resta: "+resultadoSuma);
		assertTrue(resultadoSuma == 5);
	}

	/**
	 * Test a método multiply, multiplicacion
	 */
	public void testMultiply() {
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		int resultadoSuma = servicio.multiply(7, 5);
		System.out.println("Resultado multiplicaci�n: "+resultadoSuma);
		assertTrue(resultadoSuma == 35);
	}

	/**
	 * Test a método divide, división
	 */
	public void testDivide() {
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		int resultadoSuma = servicio.divide(20, 5);
		System.out.println("Resultado divisi�n: "+resultadoSuma);
		assertTrue(resultadoSuma == 4);
	}

}
