package com.masivian.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.tempuri.Calculator;
import org.tempuri.CalculatorSoap;

import com.masivian.dao.OperacionDao;
import com.masivian.modelo.Operacion;

/**
 * Clase OperacionBean, recibe la comunicación con la vista, y la redirige al dao
 * @author Ricardo Giraldo
 *
 */
@ManagedBean (name = "operacionBean")
@RequestScoped
public class OperacionBean {
	
	//Se crean los atributos de la clase OperacionBean, temporales para obtenerlos de la vista
	private int intA;
	private int intB;
	private String tipoOperacion;
	private String resultado;
	
	
	/**
	 * Método que se ejecuta al presionar el boton ejecutar en la interfaz gráfica
	 * ejecuta el servicio de calculadora del endpoint soap
	 */
	public void ejecutar() {
		
		//Se inicializa el servicio de calculadora
		CalculatorSoap servicio = new Calculator().getCalculatorSoap();
		//Se inicializa el dao de operaciones
		//OperacionDao operacionDao = new OperacionDao();
		
		//Si los valores que llegan son válidos
		if(intA != 0 && intB != 0) {
			//Si el tipo de operación seleccionado es suma
			if("suma".equals(tipoOperacion)) {
				//se invoca el servicio add
				int resultadoSuma = servicio.add(intA, intB);
				
				//Se crea un objeto operacion para almacenar en la bd
				Operacion operacion = new Operacion();
				operacion.setIntA(intA);
				operacion.setIntB(intB);
				operacion.setTipo("Suma");
				operacion.setResultado(resultadoSuma);
				
				//Se asigna el resultado a la variable que se retorna a la vista
				resultado = "El resultado de la suma es: "+resultadoSuma;
				
				System.out.println(resultado);
				
				//operacionDao.guardar(operacion);
				
			}
			//Si el tipo de operacion seleccionado es resta
			else if("resta".equals(tipoOperacion)) {
				//se invoca el servicio substract
				int resultadoResta = servicio.subtract(intA, intB);
				
				//Se crea un objeto operacion para almacenar en la bd
				Operacion operacion = new Operacion();
				operacion.setIntA(intA);
				operacion.setIntB(intB);
				operacion.setTipo("Resta");
				operacion.setResultado(resultadoResta);
				
				//Se asigna el resultado a la variable que se retorna a la vista
				resultado = "El resultado de la resta es: "+resultadoResta;
				
				//operacionDao.guardar(operacion);
				
			}
			//Si el tipo de operacion seleccionado es multiplicacion
			else if("multiplicacion".equals(tipoOperacion)) {
				//se invoca el servicio multiply
				int resultadoMultiplicacion = servicio.multiply(intA, intB);
				
				//Se crea un objeto operacion para almacenar en la bd
				Operacion operacion = new Operacion();
				operacion.setIntA(intA);
				operacion.setIntB(intB);
				operacion.setTipo("Multiplicación");
				operacion.setResultado(resultadoMultiplicacion);
				
				//Se asigna el resultado a la variable que se retorna a la vista
				resultado = "El resultado de la multiplicación es: "+resultadoMultiplicacion;
				
				//operacionDao.guardar(operacion);
				
			}
			//Si el tipo de operacion seleccionado es division
			else if("division".equals(tipoOperacion)) {
				//se invoca el servicio divide
				int resultadoDivision = servicio.divide(intA, intB);
				
				//Se crea un objeto operacion para almacenar en la bd
				Operacion operacion = new Operacion();
				operacion.setIntA(intA);
				operacion.setIntB(intB);
				operacion.setTipo("División");
				operacion.setResultado(resultadoDivision);
				
				//Se asigna el resultado a la variable que se retorna a la vista
				resultado = "El resultado de la división es: "+resultadoDivision;
				
				//operacionDao.guardar(operacion);
				
			}
		}
	}
	
	/**
	 * método que retorna todas las operaciones almacenadas en bd
	 * @return
	 */
	public List<Operacion> obtenerOperaciones(){
		OperacionDao operacionDao = new OperacionDao();
		return operacionDao.obtenerOperaciones();
	}

	/**
	 * get int b
	 * @return
	 */
	public int getIntB() {
		return intB;
	}

	/**
	 * set int b
	 * @param intB
	 */
	public void setIntB(int intB) {
		this.intB = intB;
	}

	/**
	 * get tipo operacion
	 * @return
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * set tipo operacion
	 * @param tipoOperacion
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	/**
	 * get int a
	 * @return
	 */
	public int getIntA() {
		return intA;
	}

	/**
	 * set int a
	 * @param intA
	 */
	public void setIntA(int intA) {
		this.intA = intA;
	}

	/**
	 * get resultado
	 * @return
	 */
	public String getResultado() {
		return resultado;
	}

	/**
	 * set resultado
	 * @param resultado
	 */
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
