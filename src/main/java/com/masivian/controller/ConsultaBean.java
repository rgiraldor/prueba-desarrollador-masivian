package com.masivian.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean (name = "consultaBean")
@RequestScoped
public class ConsultaBean {
	
	private String resultado;
	
	/**
	 * Método que se ejecuta al presionar el boton ejecutar en la interfaz gráfica
	 * ejecuta el servicio de consulta del endpoint rest
	 */
	public void ejecutar() {
		
		try {

			URL url = new URL("https://jsonplaceholder.typicode.com/users");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			conn.disconnect();
			resultado = "Consulta correcta";

		  } catch (MalformedURLException e) {

			e.printStackTrace();
			resultado = "Se presentó un error";

		  } catch (IOException e) {

			e.printStackTrace();
			resultado = "Se presentó un error";

		  }
		
		
		
	}

	/**
	 * get resultado
	 * @return
	 */
	public String getResultado() {
		return resultado;
	}

	/**
	 * set resultado
	 * @param resultado
	 */
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
