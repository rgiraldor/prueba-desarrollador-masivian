package com.masivian.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad operacion
 * @author Ricardo Giraldo
 *
 */
@Entity
@Table(name = "operaciones")
public class Operacion {
	
	//Se crean los atributos de una operacion
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String tipo;
	
	@Column
	private int intA;
	
	@Column
	private int intB;
	
	@Column
	private int resultado;
	
	/**
	 * get id
	 * @return
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * set id
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * get tipo
	 * @return
	 */
	public String getTipo() {
		return tipo;
	}
	
	/**
	 * set tipo
	 * @param tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	/**
	 * get int a
	 * @return
	 */
	public int getIntA() {
		return intA;
	}
	
	/**
	 * set int a
	 * @param intA
	 */
	public void setIntA(int intA) {
		this.intA = intA;
	}
	
	/**
	 * get int b
	 * @return
	 */
	public int getIntB() {
		return intB;
	}
	
	/**
	 * set int b
	 * @param intB
	 */
	public void setIntB(int intB) {
		this.intB = intB;
	}
	
	/**
	 * get resultado
	 * @return
	 */
	public int getResultado() {
		return resultado;
	}
	
	/**
	 * set resultado
	 * @param resultado
	 */
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	
	/**
	 * Método to string
	 */
	@Override
	public String toString() {
		return "Operacion [id=" + id + ", tipo=" + tipo + ", intA=" + intA + ", intB=" + intB + ", resultado="
				+ resultado + ", getId()=" + getId() + ", getTipo()=" + getTipo() + ", getIntA()=" + getIntA()
				+ ", getIntB()=" + getIntB() + ", getResultado()=" + getResultado() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	

}
