package com.masivian.enums;

public enum TipoOeracionEnum {
	SUMA,
	RESTA,
	MULTIPLICACION,
	DIVISION;
}
