package com.masivian.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.masivian.modelo.JPAUtil;
import com.masivian.modelo.Operacion;

/**
 * Clase operacionDao, se crea para comunicarse con la bd 
 * @author Ricardo Giraldo
 *
 */
public class OperacionDao {
	EntityManager entity = JPAUtil.getEntityManagerFactory().createEntityManager();

	/**
	 * M�todo para almacenar una operaci�n en la base de datos
	 * @param operacion
	 */
	public void guardar(Operacion operacion) {
		entity.getTransaction().begin();
		entity.persist(operacion);
		entity.getTransaction().commit();
		JPAUtil.shutdown();
	}
	
	/**
	 * M�todo para obtener todas las operaciones registradas
	 */
	public List<Operacion> obtenerOperaciones(){
		List<Operacion> listaOperaciones = new ArrayList<Operacion>();
		Query consulta = entity.createQuery("SELECT o FROM Operacion o");
		listaOperaciones = consulta.getResultList();
		return listaOperaciones;
	}
	
}
