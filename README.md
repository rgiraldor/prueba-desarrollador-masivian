Prueba de desarrollador Java para masivian presentada por Ricardo Giraldo Ramírez

El proyecto fue nombrado clienteCalculadoraSoap.

Fue desarrollado en un equipo Windows de 64 bits, con las siguientes herramientas:
Eclipse IDE Version 2019-6
Java version 1.8
Apache-tomcat Version 8.5.45
Apache-maven Version 3.6.2
Mysql Version 8.0


Para desplegar el proyecto, es necesario acceder a él con ayuda del IDE Eclipse, importándolo como
un "Maven-Project", una vez importado, presionando clic derecho sobre el proyecto en la vista
lateral izquierda, y luego seleccionando la opción maven -> maven update; se pueden generar
las dependencias y arhivos de configuración necesarios para el proyecto.


Para desplegar el proyecto, sobre el IDE Eclipse bajo la perspectiva "JavaEE", seleccionando
la pestaña Window, en la barra de navegación superior, luego seleccionando ShowView->Servers;
es posible configurar el servidor apache-tomcat 8.5 sobre el cual se puede desplegar el proyceto.


Los script sql para la creación de la base de datos (MySQL) para el punto número 1 de la prueba
se detallan a continuación:

create database clienteSoap;
CREATE TABLE operaciones(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, tipo VARCHAR(20),
intA INT, intB INT, resultado INT);


Los datos de conexión a la base de datos sobre los cuales se encuentra configurado el proyecto
son los siguientes:
*Base de datos: clienteSoap
*Usuario: root
*Contraseña: 1234


Una vez desplegado el proyecto con el servidor local apache-tomcat en el puerto 8080,
se puede acceder a cada uno de los puntos siguiendo las direcciones:

*Dirección del punto número 1: Consumir endpoint soap:
http://localhost:8080/clienteCalculadoraSoap/faces/index.xhtml

*Direccion del punto número 2: Consumir endpoint rest:
http://localhost:8080/clienteCalculadoraSoap/faces/indexRest.xhtml